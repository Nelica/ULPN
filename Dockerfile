FROM ubuntu:20.04

WORKDIR /var/www/html/

# Pre instalacije...
RUN echo "UTC" > /etc/timezone
RUN apt update
RUN DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC apt-get -y install tzdata
RUN apt install ca-certificates apt-transport-https software-properties-common zip unzip curl sqlite nginx supervisor libterm-readline-gnu-perl -y
RUN add-apt-repository --yes ppa:ondrej/php
RUN apt update 

# Instalacija PHP..
RUN apt install -y php8.0 \
    php8.0-common \
    php8.0-fpm \
    php8.0-pdo \
    php8.0-opcache \
    php8.0-zip \
    php8.0-phar \
    php8.0-iconv \
    php8.0-cli \
    php8.0-curl \
    php8.0-mbstring \
    php8.0-tokenizer \
    php8.0-fileinfo \
    php8.0-xml \
    php8.0-xmlwriter \
    php8.0-simplexml \
    php8.0-dom \
    php8.0-tokenizer 

# Composer
RUN curl -sS https://getcomposer.org/installer -o composer-setup.php
RUN php composer-setup.php --install-dir=/usr/local/bin --filename=composer
RUN rm -rf composer-setup.php
RUN echo "$PWD"

# Supervisor
RUN mkdir -p /etc/supervisor/
COPY supervisord.conf /etc/supervisor/supervisord.conf

# PHP config
RUN mkdir -p /run/php/
RUN touch /run/php/php8.0-fpm.pid

COPY php-fpm.conf /etc/php8/php-fpm.conf
COPY php.ini-production /etc/php8/php.ini

# NGINX config
COPY nginx.conf /etc/nginx/
COPY nginx-laravel.conf /etc/nginx/sites-enabled/

RUN mkdir -p /run/nginx/
RUN touch /run/nginx/nginx.pid

RUN ln -sf /dev/stdout /var/log/nginx/access.log
RUN ln -sf /dev/stderr /var/log/nginx/error.log
# Composer setup
COPY laravel-8-hello-world/laravel_test/ /var/www/html/
RUN echo "$PWD"
RUN composer install --no-dev 
RUN php artisan key:generate
#RUN php artisan serve

EXPOSE 8000 80 9000
CMD ["supervisord", "-c", "/etc/supervisor/supervisord.conf"]
