# UPLN

Dockerimage za Laravel aplikaciju hello world, sa reverse proxy nginx i php8 support, bazirano na ubuntu 20.04 image.

```bash

Uraditi git clone u vas direktorijum

git clone https://gitlab.com/Nelica/UPLN.git
```
Koristite docker build . 

```bash
Nakon uspesnog builda, pokrenete container 

```

docker run -d imageid

```bash
Pristupiti containeru preko privatne IP
docker ps ( naci id )
docker inspect id | grep IPAddress
```

Primer:

```bash
            "SecondaryIPAddresses": null,
            "IPAddress": "172.17.0.2",
                    "IPAddress": "172.17.0.2",
```

Preko broswera otici na http://172.17.0.2/index.php i bice prikazana Hello world stranica, sto se i vidi u nginx logovima 

```bash
172.17.0.1 - - [10/Apr/2022:21:10:09 +0000] "GET /index.php HTTP/1.1" 200 245 "-" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.75 Safari/537.36" "-"
[Sun Apr 10 21:10:08 2022] 127.0.0.1:45432 Accepted
[Sun Apr 10 21:10:09 2022] 127.0.0.1:45432 [200]: GET /index.php
[Sun Apr 10 21:10:09 2022] 127.0.0.1:45432 Closing
```
